/*
 * Copyright 2012 Andrew Bashore
 * This file is part of GeoBot.
 * 
 * GeoBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * GeoBot is distributed in the hope that it will be useful
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with GeoBot.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.bashtech.geobot;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.RoundingMode;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.zip.GZIPInputStream;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class Tester {
	public static void main(String[] args) throws Exception {
		PrintWriter out = new PrintWriter("highnoon.txt");
		out.println("IT'S HIGH NOON (at: " + getHighNoon());
		out.close();
	}

	public static String getRemoteContent(String urlString) {

		String dataIn = "";
		try {
			URL url = new URL(urlString);
			URLConnection conn = url.openConnection();
			conn.setRequestProperty("User-Agent", "HighNoonScript");
			BufferedReader in = new BufferedReader(new InputStreamReader(
					conn.getInputStream()));
			String inputLine;
			while ((inputLine = in.readLine()) != null)
				dataIn += inputLine;
			in.close();

		} catch (Exception ex) {
			if (ex instanceof SocketTimeoutException) {
				return null;
			}
		}

		return dataIn;
	}

	public static String getHighNoon() {

		String dataIn = "";
		try {
			URL url = new URL("http://rl.se/com/zenit.py");
			URLConnection conn = url.openConnection();
			conn.setRequestProperty("user-agent", "HighNoonScript");
			conn.setRequestProperty("Referer", "http://rl.se/sub-solar-point");
			GZIPInputStream gis = new GZIPInputStream(conn.getInputStream());
			BufferedReader in = new BufferedReader(new InputStreamReader(gis,
					"UTF-8"));
			String inputLine;
			while ((inputLine = in.readLine()) != null)
				dataIn += inputLine;
			in.close();
		} catch (Exception ex) {
			if (ex instanceof SocketTimeoutException) {
				return null;
			}
			return "Coordinate API failed";
		}
		try {
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(dataIn);
			JSONArray results = (JSONArray) obj;
			double lat = (double) results.get(1);
			double lon = (double) results.get(0);
			boolean south = false;
			boolean west = false;
			double fixedlat = 0;
			double fixedlon = 0;
			if (lat < 0) {
				fixedlat = lat *= -1;
				south = true;
			}
			if (lon < 0) {
				fixedlon = lon *= -1;
				west = true;
			}
			DecimalFormat df = new DecimalFormat("#.###");
			df.setRoundingMode(RoundingMode.CEILING);
			String lat1 = df.format(lat);
			String lon1 = df.format(lon);
			String fixedlat1 = df.format(fixedlat);
			String fixedlon1 = df.format(fixedlon);
			String coordString = " - ";
			if (south) {
				coordString += fixedlat1 + "S, ";
			} else {
				coordString += lat1 + "N, ";
			}
			if (west) {
				coordString += fixedlon1 + "W";
			} else {
				coordString += lon1 + "E";
			}
			String url = "http://api.geonames.org/countrySubdivisionJSON?lat="
					+ lat1 + "&lng=" + lon1 + "&username=ends";
			try {
				Object obj1 = parser.parse(getRemoteContent(url));
				if (obj1 == null) {
					return null;
				}
				String cityName = null;
				JSONObject addObj = (JSONObject) obj1;
				if (addObj.containsKey("adminName1")) {
					cityName = (String) addObj.get("adminName1");
				}
				String countryName = null;
				if (addObj.containsKey("countryName")) {
					countryName = (String) addObj.get("countryName");
				}
				if (cityName != null && countryName != null) {
					return cityName + ", " + countryName + coordString + ")";
				} else if (countryName != null) {
					return countryName + coordString + ")";
				} else {
					return coordString;
				}
			} catch (Exception e1) {
				return coordString;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
